project(linkedlist)

set(INCLUDE_DIR ./)
include_directories (${INCLUDE_DIR})

add_executable(linkedlist main.cpp linked_list.cpp)

