#include <iostream>
#include "linked_list.h"



bool crescente(int a, int b){
  return a > b;
}

int main(int argc, char **argv) {
  List teste = createList();
  insert(teste, 8);
  insert(teste, 9);
  insert(teste, 10);
  insert(teste, 20);
  remove(teste, 10);
  insertPos(teste, 55, 4);
   
  sortList(teste, crescente);
  std::cout << showList(teste)  <<std::endl;
    return 0;
}
