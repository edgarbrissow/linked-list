#ifndef LINKED_LIST
#define LINKED_LIST
#include <string>
#include <sstream>


struct Nodo{
  int numero;
  Nodo* next;
};


struct List{
  Nodo* nodo;
  int length;
};


List createList(); //ok
List copyList(List list);
void sortList(List& list, bool (*callback)(int, int));
void insertPos(List& list, int elem, int pos); //ok
void destroiList(List& list); //ok
bool isEmpty(const List& list); //ok
bool hasSpace(); //ok
int length(const List& list); //ok
bool has(const List& list,int elem); //ok
bool isValid(const List& list, int pos);
int getElement(const List& list, int pos); //ok
int getPosition(const List& list, int elem); //ok
void insert(List& list, int elem); //ok
void remove(List& list, int elem); //ok
std::string showList(const List& list); //ok

#endif
