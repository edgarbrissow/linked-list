#include "linked_list.h"

List createList(){
  List list;
  list.length = 0;
  list.nodo = new Nodo;
  list.nodo->next = NULL;
  return list;
  
}
void insert(List& list, int elem){
list.length++;
list.nodo->numero = elem;
Nodo *p = new Nodo;
p->next = list.nodo;
list.nodo = p;
}


bool isEmpty(const List& list){
  return list.length == 0;
}

void destroiList(List& list){
  Nodo *p = NULL;
  while(list.nodo->next != NULL){
    p = list.nodo->next;
    list.nodo->next = list.nodo->next->next;
    delete p;
  }
  delete &(list);
}


bool hasSpace(){
  Nodo *p = new Nodo;
  if(p != NULL){
    delete p;
    return true;
  }else{
    return false;
  }
}

int length(const List& list){
  return list.length;
}

int getElement(const List& list, int pos){
   Nodo *p = list.nodo->next;
   for(int i = 1; i < pos; i++){
     p = p->next;
   }
   return p->numero;
}

int getPosition(const List& list, int elem){
  Nodo *p = list.nodo->next;
  int pos = 1;
  while(p != NULL){
    if(p->numero == elem)
      return pos;
    pos++;
    p = p->next;
  }
}

void insertPos(List& list, int elem, int pos){
  Nodo *p = list.nodo->next;
  int i = 1;
  while(p != NULL){
    if(i == pos)
      p->numero = elem;
    i++;
    p = p->next;
  }
  
}

bool has(const List& list,int elem){
  Nodo *p = list.nodo->next;
  while( p != NULL){
    if(p->numero == elem)
      return true;
    p = p->next;
  }
  return false;
  
}

void remove(List& list, int elem){
  Nodo *p = list.nodo;
  Nodo *del;
  while( p->next != NULL){
    if(p->next->numero == elem){
      del = p->next;
      p->next = p->next->next;
      delete del;
      list.length--;
      break;
    }
    p = p->next;
  }
  
}

void sortList(List& list, bool (*callback)(int, int)){
  int aux = 0;
  for(int i = 1; i <= length(list) ; i++ ){
    for(int j = 1; j < length(list) ; j++ ){
      if(callback(getElement(list, j), getElement(list,j+1))){
	aux = getElement(list, j);
	insertPos(list, getElement(list, j+1), j);
	insertPos(list, aux, j+1);
      }
      
    }
  }
}


std::string showList(const List& list){
  Nodo *p = list.nodo->next;
  std::stringstream s;
  while( p != NULL){
    s << p->numero << " ";
    p = p->next;
  }
  return s.str();
}

